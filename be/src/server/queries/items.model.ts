export enum ItemDataType {
  Kpi = 'kpi',
  Error = 'error'
}

export enum ItemStatus {
  None = '10',
  Passed = '0',
  Error = '1',
  Terminated = '2',
  Failed = '3'
}
