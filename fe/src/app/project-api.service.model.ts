export interface ProjectsListing {
  id: string;
  project_name: string;
}

export interface NewProjectBody {
  projectName: string;
}
