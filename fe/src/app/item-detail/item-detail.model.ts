export enum ItemStatus {
  none = '10',
  passed = '0',
  error = '1',
  terminated = '2',
  failed = '3'
}
